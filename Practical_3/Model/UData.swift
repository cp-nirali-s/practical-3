//
//  UData.swift
//  Try_3.0
//
//  Created by Nirali on 08/06/22.
//

import Foundation

struct User: Decodable, Encodable {
    
    let id: Int
    let url: String
    let login: String
    let followers: Int?
    let following: Int?
    let avatar_url: String
    
}


