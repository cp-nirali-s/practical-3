//
//  ViewController.swift
//  Practical_3
//
//  Created by Nirali on 29/06/22.
//

import UIKit
import Kingfisher
import Alamofire

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    var users = [User]()
    var apikey = "https://api.github.com/users"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(UINib.init(nibName: "UsersListTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        downloadJSON(urlString: apikey) {user in
            self.users = user
            self.tableView.reloadData()
        }
    }
    
    func downloadJSON(urlString: String, completed: @escaping ([User]) -> ()) {
        let url = URL(string: urlString)
        AF.request(url!).responseDecodable(of: [User].self) {
            response in
            if let request = response.data {
                do {
                    let user = try JSONDecoder().decode([User].self, from: request)
                    DispatchQueue.main.async {
                        completed(user)
                    }
                } catch {
                    print("JSON Error")
                }
            } else {
                print(response.error!)
            }
        }.resume()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! UsersListTableViewCell
        let userdata = users[indexPath.row]
        cell.userName?.text = userdata.login.capitalized
        
        let url = URL(string: userdata.avatar_url)
        let data = try? Data(contentsOf: url!)
        cell.userImage.image = UIImage(data: data!)
        cell.userImage.layer.cornerRadius = cell.userImage.frame.size.width / 2
        cell.userImage.clipsToBounds = true
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "showDetails", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)   {
        if let destination = segue.destination as? DetailViewController, let index = tableView.indexPathForSelectedRow?.row {
            destination.user = users[index]
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "All Users list"
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        // need to pass your indexpath then it showing your indicator at bottom
        tableView.addLoading(indexPath) {
            let lastid = self.users[indexPath.row]
            let url = "https://api.github.com/users?since=\(lastid.id)"
            if let lastUser = self.users.last, lastUser.id == self.users[indexPath.row].id {
                self.downloadJSON(urlString: url) { (user) in
                    self.users = self.users + user
                    tableView.stopLoading()
                    self.tableView.reloadData()
                }
            }
        }
    }
}

extension UITableView {
    
    func indicatorView() -> UIActivityIndicatorView{
        var activityIndicatorView = UIActivityIndicatorView()
        if self.tableFooterView == nil {
            let indicatorFrame = CGRect(x: 0, y: 0, width: self.bounds.width, height: 80)
            activityIndicatorView = UIActivityIndicatorView(frame: indicatorFrame)
            activityIndicatorView.autoresizingMask = [.flexibleLeftMargin, .flexibleRightMargin]
            
            if #available(iOS 13.0, *) {
                activityIndicatorView.style = .large
            } else {
                // Fallback on earlier versions
                activityIndicatorView.style = .whiteLarge
            }
            
            activityIndicatorView.color = .systemPink
            activityIndicatorView.hidesWhenStopped = true
            
            self.tableFooterView = activityIndicatorView
            return activityIndicatorView
        }
        else {
            return activityIndicatorView
        }
    }
    
    func addLoading(_ indexPath:IndexPath, closure: @escaping (() -> Void)) {
        indicatorView().startAnimating()
        if let lastVisibleIndexPath = self.indexPathsForVisibleRows?.last {
            if indexPath == lastVisibleIndexPath && indexPath.row == self.numberOfRows(inSection: 0) - 1 {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    closure()
                }
            }
        }
    }
    
    func stopLoading() {
        if self.tableFooterView != nil {
            self.indicatorView().stopAnimating()
            self.tableFooterView = nil
        }
        else {
            self.tableFooterView = nil
        }
    }
}

//    func downloadJSON(completed: @escaping () -> ()) {
//        let url = URL(string: "https://api.github.com/users")
//        URLSession.shared.dataTask(with: url!) { (data, response, error) in
//            if error == nil {
//                do {
//                    self.users = try JSONDecoder().decode([User].self, from: data!)
//                    DispatchQueue.main.async {
//                        completed()
//                    }
//                }
//                catch {
//                    print("JSON Error")
//                }
//            }
//        }.resume()
//    }

