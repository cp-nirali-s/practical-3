//
//  UsersListTableViewCell.swift
//  Try_3.0
//
//  Created by Nirali on 08/06/22.
//

import UIKit

class UsersListTableViewCell: UITableViewCell {

    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userImage: UIImageView!

}

