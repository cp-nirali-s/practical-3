//
//  DetailViewController.swift
//  Try_3.0
//
//  Created by Nirali on 14/06/22.
//

import UIKit
import Alamofire

class DetailViewController: UIViewController {

    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var follower: UILabel!
    @IBOutlet weak var following: UILabel!
    
    var user: User?

    override func viewDidLoad() {
        super.viewDidLoad()
            downloadJSON { [weak self] user in
                guard let followers = user.followers, let following = user.following else {return}
                self?.follower.text = "\((followers))"
                self?.following.text = "\((following))"
            }
        
        profileName.text = user?.login
        profilePic.downloadImage(with: user!.avatar_url) { image in
            if let picture = image {
                self.profilePic.image = picture }
        }
        profilePic.layer.cornerRadius = self.profilePic.frame.size.width / 2
        profilePic.clipsToBounds = true
    }
   
        func downloadJSON(completed: @escaping (User) -> ()) {
            guard let urlstr = user?.url, let url = URL(string: urlstr) else {return}
            AF.request(url).responseDecodable(of: [User].self) {
                    response in
                if let request = response.data {
                    do {
                        let profileData = try JSONDecoder().decode(User.self, from: request)
                        DispatchQueue.main.async {
                            completed(profileData)
                        }
                    } catch {
                        print("JSON Error")
                    }
                } else {
                    print(response.error!)
                }
            }.resume()
    }
        
}

//        func downloadJSON(completed: @escaping (User) -> ()) {
//
//            guard let urlstr = uro?.url, let url = URL(string: urlstr) else {return}
//            URLSession.shared.dataTask(with: url) { (data, response, error) in
//                if error == nil {
//                    do {
//                        let pqr = try JSONDecoder().decode(User.self, from: data!)
//                        DispatchQueue.main.async {
//                            completed(pqr)
//                        }
//                    }
//                    catch {
//                        print("JSON Error")
//                    }
//                }
//            }.resume()
//        }
        
